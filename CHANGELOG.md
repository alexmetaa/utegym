# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] - 2021-07-06

### Added
- Utkast till första version
- Lagt upp Markdown-mall som öppen källkod på [GitLab](https://gitlab.com/lankadedata/specifikationer/utegym)
- Revisionshistorik finns nu tillgänglig på [CHANGELOG](https://gitlab.com/lankadedata/specifikationer/utegym/-/blob/master/CHANGELOG)

### Changed
- 

### Fixed
- 

### Removed
- 

[Unreleased]: 
