# Specifikation för Utegym

Information om utegym, särskilt anordnade.

# Byggscript

Ett minimalt byggsteg behövs för att sätta samman index.html filen.

> ./build.sh

# Kom igång och bidra

Läs instruktioner på sidan [CONTRIBUTING](https://gitlab.com/lankadedata/spec/utegym/-/blob/master/CONTRIBUTING.md).