## Exempel i CSV, Semikolonseparerad

Nedan är första raden i en fil som representerar Finnsjö utegym i Härryda Kommun. Observera att flera fält är tomma och då helt saknar innehåll, istället är de tänkta värdena ersatta med ett semikolon.

Flera värden är tomma, t.ex. adressfälten eftersom platsen saknar gatunamn och husnummer, men däremot finns postadressen Härryda, så den är ifylld. Om du inte vet ifall något finns på platsen, t.ex. “rings” så ska du inte skriva “false” utan du ska lämna fältet tomt. Tomt = vet ej/ej inventerat. 

Om du har flera fält som du lämnar tomma, kanske det är dags att kommunen inventerar utegymmet på nytt för att kunna förse datakonsumenten med så aktuell data som möjligt. 

<div class="example csvtext">
source;id;name;latitude;longitude;email;visit_url;wikidata;updated;description;street;housenumber;postcode;city;country;lantern;sit-up;push-up; paralell_bars;horizontal_ladder;balance_beam;horizontal_bar; battling_ropes;captains_chair;slackline;rower;air_walker; exercise_bike; rings;elliptical_trainer;hurdling;log_lifting;box;sign<br>
1401;0180-Finnsjo;FinnsjosUtegym;57.640382;12.136265;fritid@harryda.se;https://www.harryda.se/upplevaochgora/idrottmotionochfriluftsliv/motionochfriluftsliv/utegym.4.3500511a14601bad3c4b40.html;Q107213274;2021-06-22;Finnsjös utegym består av 9 redskap vardera med möjlighet till mångsidig och varierad träning. Beläget nära Finnsjögården.;;;Härryda;SE;true;false;true;false;true;false;true;false;true;false;false;false;true;true;true;false;true;true;true
</div>