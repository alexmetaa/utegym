# Exempel i JSON

Samma utegym som i Appendix A, fast nu som JSON.

```json
{
    "source": "1401"
    "id":  "0180-Finnsjo",
    "name":  "Finnsjos Utegym",
    "latitude":  "57.640382",
    "longitude":  "12.136265",
    "email":  "fritid@harryda.se",
    "visit_url":  "https://www.harryda.se/upplevaochgora/idrottmotionochfriluftsliv/motionochfriluftsliv/utegym.4.3500511a14601bad3c4b40.html",
    "wikidata":  "Q107213274",
    "updated":  "2021-06-22",
    "description":  "Finnsjös utegym består av 9 redskap vardera med möjlighet till mångsidig och varierad träning. Beläget nära Finnsjögården. ",
    "street":  "",
    "housenumber":  "",
    "postcode":  "",
    "city":  "Härryda",
    "country":  "SE",
    "lantern":  "true",
    "sit-up":  "false",
    "push-up":  "false",
    "paralell_bars":  "false",
    "horizontal_ladder":  "false",
    "balance_beam":  "false",
    "horizontal_bar":  "true",
    "battling_ropes":  "false",
    "captains_chair":  "false",
    "slackline":  "false",
    "rower":  "false",
    "air_walker":  "false",
    "exercise_bike":  "true",
    "rings":  "true",
    "elliptical_trainer":  "true",
    "hurdling":  "true",
    "log_lifting":  "true",
    "box":  "true",
    "sign":  "true",

}
```