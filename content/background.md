
**Bakgrund**

Det finns behov av att presentera grundläggande information om utegym i Sverige på ett likvärdigt och enkelt sätt som öppna data. Ambitionen är även att den datan som publiceras enligt denna specifikation ska kunna länkas till Wikidata och övriga liknande initiativ som har koppling till utegym. 





