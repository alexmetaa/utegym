# Beskrivning i datakatalog

Om du önskar publicera din skapade datamängd i en katalog, ska du använda dig av [W3C-rekommendationen DCAT](https://www.w3.org/TR/vocab-dcat/). För svenska behov är det [DCAT-AP-SE](https://lankadedata.se/spec/DCAT-AP-SE/) som gäller. För att göra det enklare att hitta alla datamängder som följer specifikationen på t.ex. [dataportal.se](https://dataportal.se/) är det viktigt att man markerar dessa datamängder. Man bör då göra följande:

1. På datamängden peka ut denna specifikation via propertyn dcterms:conformsTo (med fältnamnet "uppfyller").
2. Lägg till minst "utegym" som nyckelord på svenska.
3. På distributionen som motsvarar CSV filen peka ut schemat via propertyn dcterms:conformsTo (med fältnamnet "länkade scheman").
4. Sätt rätt språk, det språk som ditt data är skrivet på via propertyn dcterms:language med fältnamnet “Språk”. Standard torde vara Svenska. Du som har behov att att publicera data på fler språk lägger till flera distributioner som i sin helhet är författade på det språk du ämnar dela.

## Minsta krav på datamängdens metadata

I i specifikationen [DCAT-AP-SE](https://www.w3.org/TR/vocab-dcat/) anges [vilka fält som måste anges när man beskriver en datamängd](https://docs.dataportal.se/dcat/sv/#dcat%3ADataset), då denna specifikation skrivs är det titel, beskrivning och utgivare som är obligatoriska. Utöver dessa fält rekommenderas det att följande fält fylls i:

1. **Uppdateringsfrekvens** eller **ändringsdatum** - om datamängden uppdateras regelbundet eller automatsikt räcker det att man anger en uppdateringsfrekvens. Om det sker oregelbundet är det lämpligt att man sätter ett ändringsdatum. Detta enligt [rekommendation 7 på dataportalens dokumentation](https://docs.dataportal.se/dcat/docs/recommendations/#7-utgivningsdatum-modifieringsdatum-och-uppdateringsfrekvens).(*)
    
2. **Kontaktuppgift** - en kontaktväg för frågor/synpunkter kring datat, det kan vara en bevakad postlåda eller tjänst för att konsumenter ska komma i kontakt med dataproducenten.

(*) Observera att du alltid också ska uppdatera fältet “updated” inne i datat på varje rad i filen där något ändrats.

För att ditt data ska möta förväntade behov och vara till nytta för en datakonsument, måste diskussion om datat vara möjligt - felanmälan, frågor eller förbättringsförslag måste kunna tas emot och värderas. Öppna Data måste vara “levande” och i dialog med den som konsumerar datat kan du skapa en bättre, mer korrekt och därmed en bättre nyttjad datamängd.

## Licens

Licensen för din datamängd [uttrycks på distributionensnivån](https://docs.dataportal.se/dcat/sv/#dcat_Distribution-dcterms_license).

Licensen för data som släpps enligt denna specifikation bör vara CC0, den helt fria och öppna licensen för Creative Commons. **_Detta är en stark rekommendation och annan licensform bör motiveras och användas endast i sällsynta undantagsfall!_**

CC0 betyder att inga kostnader, begränsningar eller hinder finns för vem som helst att använda, transformera, länka och hantera datat utan krav på motprestation. Läs mer på [Creative Commons](https://creativecommons.org/about/cclicenses/) om de olika licenser som ingår i definitionen.

För att datat denna specifikation definierar ska få maximal spridning, användning och ge störst nytta måste den släppas under en fri licens och inte hämmas av krav. 
