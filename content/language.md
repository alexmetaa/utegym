# Språkstöd

En brist i den här specifikationen är avsaknad av i specifikationen inbyggt stöd för olika språk för fritextfält. Du som har behov att publicera din data på flera språk behöver publicera en översättning av hela datamängden. Du uppmanas att med DCAT-AP i din Öppna Data -katalog att ange vilket språk din data är publicerad på. På så vis kan du publicera data på vilka språk du vill. Se även Appendix D för förbättringsidéer för version 2.0 av specifikationen.

Ange språk på property `dcterms:language` för din distribution, fältnamnet är “Språk” i din DCAT-AP-SE -metadata. Ange språkkod på attribut 
