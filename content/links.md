# Länkar

## WikiData

På initiativet Wikidata ([https://www.wikidata.org](https://www.wikidata.org)) görs ett gediget arbete för att kunna länka och bygga kunskap utefter data. Genom att göra den här specifikationen kompatibel med Wikidata genom länkning hoppas vi kunna bidra till att höja kvaliteten i Wikidata och i specifikationen. Vill du att dina utegym i kommunen syns bättre i dessa plattformar - se till att bilder på utegym finns tillgängliga med med fri licens som CC-0 och/eller ladda upp bilder på [WikiCommons](https://commons.wikimedia.org/wiki/Main_Page).

Använd fältet “wikidata”, egenskap nummer 8 i denna specifikation, för att länka till den det objekt i Wikidata som beskriver utegymmet. Det kan komma att finnas ett ID i Wikidata för varje utegym.


Wikidata-id anges med en så kallad “Q-kod” och börjar med bokstaven Q och följs sedan av ett antal siffror. Detta är det unika ID som utegymmet har i Wikidatas databas. Om det inte finns en Q-kod för ditt bad, rekommenderas du att lägga till utegymmet på Wikidata och länka dit.

Om utegymmet finns med i Wikidata öppnas fler möjligheter och rekommendationen är att du skapar ett objekt där som beskriver ditt utegym. Wikidata för att t.ex. rita en karta och länka till andra datakällor på Wikidata gällande svenska utegym: [https://w.wiki/3VsD](https://w.wiki/3VsD) Se projektet på Wikidata Wikidata:WikiProject_Outdoor_Gyms [https://www.wikidata.org/wiki/Wikidata:WikiProject_Outdoor_Gyms](https://www.wikidata.org/wiki/Wikidata:WikiProject_Outdoor_Gyms) där även bilder på utegym kopplas till gymmet

## Open Street Map

Under arbetet med denna specifikation valde vi att utgå från Open Street Maps mappning gällande utegym och dess träningsstationer. Vi önskade att specifikationen för utegym blir kompatibel med och kan användas av gemenskapen på bästa sätt. 

## Tillgänglighetsdatabasen

Tillgänglighetsdatabasen saknar persistenta identifierare som är stabila över tid och det går inte att hänvisa till ett objekt på ett stabilt sätt annat än med hjälp av URL direkt till objektet som beskrivs. Tips har lämnats till TD om att en sådan identifierare kanske kunde skapas och göras synlig/tydlig i API och på webbplatsen, så att länkning av data kan ske på ett bra sätt. 

TD har ett API som du kan nå på
[Tillgänglighetsdatabasen - developer portal (azure-api.net)](https://td.portal.azure-api.net/) och som är kostnadsfritt. För att använda API:erna måste du dock registrera dig och hämta ut en personlig nyckel. APIt och dess utveckling sköts helt av Tillgänglighetsdatabasen och frågor om dess kapabilitet och funktion hänvisar vi till TD. Lämna gärna förbättringsförslag till TD som har indikerat till oss att de är intresserade av att deras tjänster nyttjas och utvecklas

### Mitt utegym saknas i Tillgänglighetsdatabasen

Kontakta din kommun där utegymmet ligger och be dem lägga till utegymmet. Generellt saknas många utegym.