# Datamodell

Datamodellen är tabulär, där varje rad motsvarar exakt ett uteplats och varje kolumn motsvarar en egenskap för denna plats. 42 attribut är definierade, där de första 4 är obligatoriska. För att förenkla länkning av data och separation av avsändare av datat, är kolumnen “id” alltid obligatorisk. Detta ID måste vara unikt och beständigt över tid och får inte återanvändas om ett utegym ex. läggs ned. Se sektion 2.1 för förtydliganden.

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Orsaken till detta är att man vill använda kolumnnamn snarare än ordningen för att detektera om viss data finns. Korta och enkla kolumnnamn minskar risken för att problem uppstår. Det är vanligt att man erbjuder söktjänster där frågor mot olika kolumner skrivs in i webbadresser och det är bra om det kan göras utan speciella teckenkodningar.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.
</div>

<div class="note" title="2">
Kolumn 3 och 4, latitude och longitude, anges på “WGS84-format” (se kapitel 2.1). Detta är det breda vedertagna koordinatsystem som används av många kommersiellt tillgängliga kart- och  navigationsverktyg. Det är enkelt att genom flera av dessa verktyg att ange eller plocka ut koordinater för en plats i detta format, varför vi har valt det som attribut för position.

För dataproducenten kan rekommenderas <a href="https://www.openstreetmap.org/">OpenStreetMap</a> för att t.ex. hitta koordinater för en viss plats genom att placera en kartnål och kopiera kartnålens koordinat.

Den som kan använder koordinater från kommunens GIS-system - tala med din GIS/Kart- och mätenhet och tillse att du om möjligt använder samma koordinater i din datamängd som finns i kommunens övriga system
</div>

<div class="ms_datatable">

| #  | Kolumnnamn      | Datatyp                         | Exempel och förklaring |
| -- | --------------- | ------------------------------- | ---------------------- |
| 1  | source          | heltal                          | **Obligatoriskt** - Ange SCB:s kommunkod för er kommun. Exempel: "0163" för Sollentuna kommun. |
| 2  | id              | text                            | **Obligatoriskt** - Ange en unik och stabil identifierare för utegymmet inom er kommun. Det viktiga är att identifieraren är unik (tänk unik i hela världen), och att den är stabil över tid - dvs att den inte ändras mellan uppdateringar. Om ett utegym avvecklas ska identifieraren inte återanvändas på ett annan utegym. Läs mer om detta attribut i sektion 2.2 <br><br>|
| 3  | name            | text                            | **Obligatoriskt** - Utegymmets namn, exempel för ett utegym i Göteborgs Stad “Backavallen utegym” eller för ett utegym i Stockholms Stad “Björkhagens utegym” <br><br>|
| 4  | latitude        | decimaltal                      | **Obligatoriskt** - Latitud anges per format som WGS84-specifikation. WGS84 är den standard som det Amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En latitudangivelse som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. <br><br>|
| 5  | longitude       | decimaltal                      | **Obligatoriskt** - Longitud anges per format som WGS84. Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges. <br><br>|
| 6  | email           | text                            |**Obligatoriskt** - E-postadress för vidare kontakt, anges med gemener och med @ som avdelare. Använd ej mailto: eller andra HTML-koder. Exempel: info@toreboda.se <br>E-postadress ska anges enligt formatspecifikation i RFC5322 i sektionen “addr-spec”. Verifiera att du följer denna standard.|
| 7  | visit_url       | URL                             |  Länk till allmän besöksinformation för platsen, t.ex. turistinformation eller liknande. Ange lämpligen den sida för utegym på kommunens webbsida. Det här är den ingångssida som du önskar att besökare skall komma till för att lära sig mer om utegym i hela kommunen. Exempel på en sådan sida i Göteborgs Stad: [Utegym och hinderbanor - Göteborgs Stad](https://goteborg.se/wps/portal/start/kultur-och-fritid/fritid-och-natur/motion-och-halsa/utomhusidrott-motion/utegym/)<br><br>Vi rekommenderar starkt att du använder detta attribut för att leda besökare till mer information kring utegym i kommunen.|
| 8  | wikidata        | datum                           | Referens till wikidata-artikel/entry om platsen. Se kapitel “3. Wikidata" i detta dokument. Använd den sk. Q-koden till entiteten på Wikidata.<br><br>Exempel för att länka till Askimsbadets utegym i Göteborgs Stad på Wikidata:<br>Q107213254<br><br>Q-kod anges utan mellanslag, bindestreck eller skiljetecken och består alltid av bokstaven Q följt av ett antal siffror. <br><br>|
| 9  | updated         | datum                           | Ett datum som anger när informationen om utegymmet senast uppdaterades. Detta attribut ska anges när någon del av informationen har uppdaterats, hur liten och till synes obetydlig uppdateringen är. Utan datum blir det svårt för en implementatör att veta hur aktuell datat är och ifall det går att lita på. Datum ska anges på format enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html) utan undantag.<br><br>Exempel, onsdagen den 21:e juli 2021 ska anges enligt:<br>2021-07-21<br><br>Var noga med att uppdatera detta fält när du gör en ändring, hur liten den än är, på ett utegym. <br><br>|
| 10  | description    | text                            | Kort beskrivning av platsen och omgivningen. Undvik att beskriva sådant som redan framgår i specifikationen, t.ex. behöver du inte skriva att utegymmet har en sit-ups station då det framgår genom att sätta true i attribut nr 15 “ sit-up”.<br><br>Exempel på beskrivning: “Utegym beläget nära Askimsbadet med tillgång till badplats”.<br><br>Vill du dela data på flera olika språk? Se sektion 5 och 7 för att lära dig mer om kraven på metadata i samband med publicering. <br><br>|
| 11 | street          | text                            | Gatuadress till platsen, exempelvis “Strandvägen”. Ange inte nummer på gata eller fastighet, det anges i attributet “housenumber”. Mappar mot Open Street Maps “addr:street” läs mer på [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 12 | housenumber     | text                            | Gatunummer eller husnummer med bokstav. Exempelvis “1” eller “42 E”. Mappar mot Open Street Maps “addr:housenumber” läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 13 | postcode        | text                            | Postnummer, exempelvis “54191”, mappar mot Open Street Maps “addr:postcode”, läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 14 | city            | text                            | Postort, exempelvis “Töreboda”. Mappar mot Open Street Maps “addr:city”, läs mer på: [Key:addr på Open Streetmaps webbplats.](https://wiki.openstreetmap.org/wiki/Key:addr) <br><br>|
| 15 | country         | text                            | Land där utegymmet finns. Skall anges enligt [ISO3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2). För Sverige ange “SE”. Fältet mappar mot Open Streetmaps “addr:country”. <br><br>|
| 16 | lantern         | boolean                         | Beskriver om utegymmet har belysning.|
| 17 | sit-up          | boolean                         | Beskriver om utegymmet har en station för att utöva sit-ups.<br>https://wiki.openstreetmap.org/wiki/File:Inclined_sit-up_bench.jpg |
| 18 | push-up         | boolean                         | Beskriver om utegymmet har en träningsstation för lutande armhävningar.<br>https://wiki.openstreetmap.org/wiki/File:Push-up_bars.jpg.|
| 19 | paralell_bars   | boolean                         | Beskriver om utegymmet har två horisontella stänger sida vid sida för att utföra exempelvis dips eller andra typer av övningar. <br>https://wiki.openstreetmap.org/wiki/File:Sente_%C3%A0_Sant%C3%A9_Trim_Trail_Jersey_2012_11.jpg|
| 20 |horizontal_ladder| boolean                         | Beskriver om utegymmet har en serie korta horisontella stänger monterade sida vid sida som är tillräckligt höga för att utföra t.ex. armgång.<br>https://wiki.openstreetmap.org/wiki/File:Horizontal_ladder.jpg|
| 21 | balance_beam    | boolean                         | Beskriver om utegymmet har en horisontell balk för att utföra balansgång eller annan typ av balansträning.<br>https://wiki.openstreetmap.org/wiki/File:Exercise9170.JPG.|
| 22 | horizontal_bar  | boolean                         | Beskriver om utegymmet har en horisontell stång som är tillräckligt hög för pull-ups, muscle-ups och andra typer av övningar.<br>https://wiki.openstreetmap.org/wiki/File:Trimm-Dich-Pfad_Gr%C3%BCnwalder_Forst_Klimmz%C3%BCge.jpg.|
| 23 | battling_ropes  | boolean                         | Beskriver om utegymmet har ett eller flera viktade rep. https://wiki.openstreetmap.org/wiki/File:Battling_ropes.jpg|
| 24 | captains_chair  | boolean                         | Beskriver om utegymmet har en stol med endast rygg och armbågsstöd (ingen sits) för att utföra benlyft.<br>https://wiki.openstreetmap.org/wiki/File:Captains_chair.jpg|
| 25 | slackline       | boolean                         | Beskriver om utegymmet har ett rep eller band monterat mellan två punkter för balansövningar.<br>https://wiki.openstreetmap.org/wiki/File:Slacklineanlage_im_Dietenbachpark.jpg|
| 26 | rower           | boolean                         | Beskriver om utegymmet har en träningsstation med ett rörligt säte och antingen två vertikala handstänger eller ett rep med handtag för att simulera en roddrörelse.<br>https://wiki.openstreetmap.org/wiki/File:Annecy_-_rower.jpg|
| 27 | air_walker      | boolean                         | Beskriver om utegymmet har en träningsstation med två svängande plattformar för fötterna och en stödstång för händerna.<br>https://wiki.openstreetmap.org/wiki/File:Ansan_Park_(beind_the_city_hall)_022.JPG|
| 28 | exercise_bike   | boolean                         | Beskriver om utegymmet har en eller flera träningscyklar.<br>https://wiki.openstreetmap.org/wiki/File:Outdoor_stationary_bicycle.jpg|
| 29 | rings           | boolean                         | Beskriver om utegymmet har två upphängda ringar för gymnastiska övningar.<br>https://wiki.openstreetmap.org/wiki/File:Exercise9159.JPG|
| 30 | elliptical_trainer| boolean                       | Beskriver om utegymmet har en träningsstation för kardiovaskulär träning, ex. en cross trainer.<br>https://wiki.openstreetmap.org/wiki/File:07-12-2016,_Fitness_trail,_Parque_da_Alfarrobeira,_Albufeira_(2).JPG|
| 31 | hurdling        | boolean                         | Beskriver om utegymmet har ett eller flera liknande hinder för att springa eller hoppa över, ex. häckar.<br>https://wiki.openstreetmap.org/wiki/File:Fitness_station_hurdling.jpg|
| 32 | log_lifting     | boolean                         | Beskriver om utegymmet har en träningsstation med en eller flera stockar med ett handtag för att utföra ex. marklyft.<br>https://wiki.openstreetmap.org/wiki/File:Log_lifting_exercise.jpg|
| 33 | box             | boolean                         | Beskriver om det finns en fixerad plattform för ex. upphopp.<br>https://wiki.openstreetmap.org/wiki/File:Exercise_Box.jpg|
| 34 | sign            | boolean                         | Beskriver om det finns en skylt som visar övningar man kan utföra vid en träningsstation eller på marken.<br>https://wiki.openstreetmap.org/wiki/File:Outdoor_fitness_station_sign.jpg|
| 35 | td_url          | URL                             | Länk till Tillgänglighetsdatabasen, TD. Det finns inga unika IDn att länka till utan du länkar till hela det URL som går direkt till ett objekt/plats som är upplagd i Tillgänglighetsdatabasen. Se sektion NNN i detta dokument för mer information om Tillgänglighetsdatabasen.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.

### **decimaltal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas istället (då undviks problem med CSV formatet som använder komma som separator).

Den kanoniska representationen i xsd:decimal är påbjuden, dvs inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**
En URL är en webbadress enligt specifikationen RFC 1738. I detta sammanhang förutsätts schemana http eller https. För webbadresser med speciella tecken tillåts UTF-8-encodade domäner och pather, se då RFC 3986.

Observera att man inte får utelämna schemat, dvs "www.example.com" är inte en tillåten webbadress, däremot är "http://www.example.com" ok. Relativa webbadresser accepteras inte heller. (En fullständig regular expression utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att parsa, dvs “läsa in” din fil utan problem. Det finns flera onlineverktyg, t.ex. https://csvlint.io som du kan använda för att testa din fil.

### **boolean**
Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad “Boolesk” datatyp och kan antingen ha ett av två värden: Sant eller Falskt, men aldrig båda.

### **phone**
Reguljärt uttryck: **`/^\+?[\d-]{6,12}$/`**

För bästa internationella funktion rekommenderas att ange telefonnummer i internationellt format enligt [RFC3966](https://www.ietf.org/rfc/rfc3966). Då anger du landskod med + före riktnummer, utan inledande nolla på riktnummer. Telefonnummer till Töreboda kommuns växel, 0506-18000 anges internationell på följande sätt: `+46-506-18000`

Andra godtagbara och möjliga format, istället för internationellt nummer: `0506-180`

**Observera** att man inte får inkludera mellanslag i telefonummret då man vill kunna generera en webbadress (URI) utifrån numret vilket inte går om det är mellanslag i det.

## Förtydligande kring attributet id
Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Observera att “id” inte är namnet på platsen utan dess ID. I fältet “name” skriver du in precis hur du vill att utegymmet ska representeras i text. “ id” är enbart till för att skapa en stabil identifiering som används när man ska låta en dator hantera informationen. Du kan jämföra attributen “id” och “name” som personnummer och namn för en människa. id är personnummer och identifierar exakt en person. "name" är namn på personen och flera människor kan heta Ellen Ripley, men alla har ett unikt personnummer. Det är mycket viktigt att detta attribut är stabilt över tid. Det ska aldrig ändras. Om ett utegym flyttas, stängs eller byter namn är det OK att skapa ett nytt id men tillse att det gamla IDt slutar att existera och aldrig återanvänds.


Om du redan har en unik och stabil identifierare för ditt utegym i ett eget system, t.ex. ett GIS-system, ett anläggningsregister för fritidsområden, ett beskrivningsID för platsen i ett skötselsystem för en parkavdelning eller motsvarande, kan du använda denna istället som id. Du ansvarar för att namnet är unikt och stabilt över tid. Ett ID från ett verksamhetssystem inom kommunen duger gott, om du tillser att det är just unikt. Tre exempel på IDn från verksamhetssystem inom en fiktiv kommun:

* urn:ngsi-ld:Outdoor_gym:se:gullspang:anlaggning:108 
* toreboda.se-anlaggningsid-BC110-22031
* goteborg-utegym-Y031askimsbadetsutegym

Om du inte har en unik och stabil identifierare för ditt utegym redan, rekommenderas du att skapa en genom att ange en kommunkod från SCB för den kommun där utegymmet finns, följt av ett bindestreck “-” och namnet på utegymmet utan mellanslag och utan svenska tecken. Använd A-O och a-o, inga apostrofer, accenter, skiljetecken. Kommunkod anges alltid med fyra siffror med inledande nolla om sådan finns: <br>
`[kommunkod]-[NamnUtanSvenskaTeckenEllerMellanslag]`

Uttryckt som ett reguljärt uttryck blir detta: **`/^\d{4}-[a-zA-Z]+$/`**

Exempel “0180-FinnsjoUtomhusgym” 

För utegym utanför Sverige som saknar kommunkod, anger du 9999. Exempel för ett utegym i Norge: 9999-KjelsasUtendorsTreningspark

Unik och stabil identifierare för utegym. Består förslagsvis av kommunkoden (4 siffror), ett bindestreck “-” och sedan namnet på utegymmet utan Å, Ä, Ö, bindestreck, mellanslag eller andra tecken som apostrof etc.. Endast bokstäver a-z och A-Z är tillåtet.

Exempel på icke tillåtna namn:

* 0180-Finnsjo utomhusgym
* 0180-FinnsjöUtomhusgym
* 0180-Finnsjö Utomhusgym Finnsjövägen 10
* Utegym 002 Härryda Kommun
* Finnsjös Utomhusgym Härryda i Sverige
* 021 023 - Anläggning 12
* harryda.se/utegym/Finnsjö utomhusgym

Ovanstående namn korrigerade att bli giltiga:

* 0180-FinnsjoUtomhusgym
* 0180-FinnsjoUtomhusgym
* 0180-FinnsjoUtomhusgymFinnsjovagen
* Utegym002HarrydaKommun
* FinnsjosUtomhusgymHarrydaiSverige
* 021_023_Anlaggning_12
* harryda.se/utegym/finnsjo_utomhusgym
